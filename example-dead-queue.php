<?php

require 'vendor/autoload.php';

use \LeoCarmo\RedisQueue\Listener;
use \LeoCarmo\RedisQueue\DeadQueue;

$redis = new Redis();
$redis->connect('localhost');

$queue = 'score:events';

//RedisQueue::setDefaultQueueClient($redis);
Listener::setQueueClient($queue, $redis);

echo DeadQueue::countMessagesInQueue($queue) . PHP_EOL;
DeadQueue::restoreMessages($queue);
echo DeadQueue::countMessagesInQueue($queue) . PHP_EOL;

dump(DeadQueue::getMessagesFromQueue($queue, 0, 9));

dump(DeadQueue::removeMessagesFromQueue($queue));
dump(DeadQueue::getMessagesFromQueue($queue, 0, 9));
echo DeadQueue::countMessagesInQueue($queue) . PHP_EOL;



