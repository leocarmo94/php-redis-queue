<?php

require 'vendor/autoload.php';

use \LeoCarmo\RedisQueue\Publisher;

$redis = new Redis();
$redis->connect('localhost');

//RedisQueue::setDefaultQueueClient($redis);
Publisher::setQueueClient('score:events', $redis);

while (true) {
    Publisher::pushMessage('score:events', [
        'event' => rand(1, 3),
        'data' => 'score',
    ]);
}