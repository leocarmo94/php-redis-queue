<?php

require 'vendor/autoload.php';

use \LeoCarmo\RedisQueue\Listener;

$redis = new Redis();
$redis->connect('localhost');

//RedisQueue::setDefaultQueueClient($redis);
Listener::setQueueClient('score:events', $redis);

class EventsProcess
{

    public function __invoke($events)
    {
        foreach ($events as $event) {
            dump($event);
        }

        //throw new \Exception('Fail');
    }
}

$start = microtime(true);
Listener::restoreMessagesFromProcessingQueue('score:events', 1);

$callback = new EventsProcess();

while (true) {
    Listener::processMessages('score:events', 1, 100, $callback);
}
echo 'Tempo: ' . round((microtime(true) - $start) * 1000);